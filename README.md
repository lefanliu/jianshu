
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0805/101729_e3f51ccb_1522177.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)

### 最新课程


---
> ### 连享会 现场班-精品课程
  
> #### A. [2020寒假Stata现场班](https://gitee.com/arlionn/Course/blob/master/StataFull.md) 
> **时间地点：** 2020年1月8-17日，北京         
> **主讲嘉宾：** 连玉君 (中山大学)；江艇 (中国人民大学)                   
> &emsp; 完整的知识架构是你长期成长的动力源泉……



> #### B. [连享会-文本分析与爬虫专题班](https://gitee.com/arlionn/Course/blob/master/Done/2020Text.md)   
> **时间地点：** 2020年3月26-29日，西安-西北工业大学     
> **主讲嘉宾：** 司继春(上海对外经贸大学)；游万海(福州大学)


&emsp;


### Stata 基本设定\Stata资源\FAQs

#### Stata 资源
- [连享会新命令 lxh：随时查看 Stata 资源](http://www.jianshu.com/p/1a2ac26a8ca8)
- [徐现祥教授团队 IRE 公开数据：官员交流、方言指数等](http://www.jianshu.com/p/5e97a7c166af)
- [Amazing！在线浏览 Stata Journal 单篇论文 (2001-2019)](http://www.jianshu.com/p/2cfbb81ac693)
- [在线浏览 Stata 15 PDF 全套电子手册](https://www.jianshu.com/p/8b48a32219b8)
- [哇！Stata 书库来袭！](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/f1c4b8762709)
- [Stata 相关书籍\课本汇总](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/f1c4b8762709)
- [Stata帮助和网络资源汇总(持续更新中)](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/c723bb0dbf98)
- [分享：Princeton 在线 Stata 教程](https://www.jianshu.com/p/0d302ba2bdab)
- [UCLA Stata FAQ：Stata常见问题详解](http://www.jianshu.com/p/c5f62bba68f7)
- [Cameron 教授提供的 Stata 资源](https://www.jianshu.com/p/b679403c795a)
- [Stata Blogs - Econometrics By Simulation](http://www.jianshu.com/p/819f9b1fc0f8)
- [Stata: 分享一些 Stata 资源链接](http://www.jianshu.com/p/6cbffff201a6)
- [Github上的 Stata 资源-持续更新中](http://www.jianshu.com/p/dad754ea953e)
- [Github使用方法及Stata资源](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/d2ee76b0f74c)
- [Stata连享会：分享一大堆资料](http://www.jianshu.com/p/21b762c6fce4)
- [在线统计课本分享：online statistics book](http://www.jianshu.com/p/8704a39a5021)
- [分享：一个在线 Stata 教程网站](http://www.jianshu.com/p/b05e42c0d866)


#### Stata 设定和外部命令
- [Stata快捷键GIF：键盘就是你的武器](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/813568a223e2)
- [Stata 中 Do-file 编辑器的使用](http://www.jianshu.com/p/567947f53955)
- [Stata: 聊聊 profile.do 文档](http://www.jianshu.com/p/387118287099)
- [Stata: 外部命令的搜索、安装与使用](http://www.jianshu.com/p/9b8ecf8f332e)
- [Stata: 史上最全的外部命令一览](http://www.jianshu.com/p/011b6846793c)
- [Stata Plus：连老师的 Stata 外部命令集](http://www.jianshu.com/p/c1e9f627d8e1)
- [Stata15 Unicode：一次性转码，解决中文乱码问题](https://www.jianshu.com/p/4347685cc11e)
- [Stata 15 dofile 和 .dta 文件转码方法](https://www.jianshu.com/p/149a0f95deaa)
- [Stata: 苹果(MAC)用户无法使用 shellout 命令？](http://www.jianshu.com/p/dfd034d1f00e)
- [Stata logo：一些漂亮的字符画](http://www.jianshu.com/p/880af5f72acb)
- [Stata: 快速呈现常用分布临界值表](http://www.jianshu.com/p/e3cfc559eaa2)
- [Stata15某些版本无法自动生成log文件问题](https://www.jianshu.com/p/8a0353c961cf)
- [Stata兄弟：提问时能否有点诚意？用 dataex 吧](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/9870080fe769)
- [第一届stata中国用户大会嘉宾PDF讲稿下载](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/db46fae3fd95)
- [Stata dofile 转换 PDF 制作讲义方法](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/b119033d8b93)
- [在 Linux 上安装 Stata](http://www.jianshu.com/p/b0dabde90442)

### 数据处理

#### 专题教程 \ 财务指标
- [数据分析修炼历程：你在哪一站？](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/7382f9f57605)
- [普林斯顿Stata教程 - Stata数据管理](http://www.jianshu.com/p/f0aa1d0bcf79)
- [Stata小白系列之一：调入数据](https://www.jianshu.com/p/273ab0a9f3bb)
- [Stata小白系列之二：数据拆分与合并](http://www.jianshu.com/p/8d193ffdde05)
- [Stata：各类盈余管理指标Stata实现方法](http://www.jianshu.com/p/68e857350248)
- [盈余管理、过度投资怎么算？分组回归获取残差](https://blog.csdn.net/arlionn/article/details/89765063)
- [Stata：因子变量的全攻略](http://www.jianshu.com/p/16b08797e591)

#### 数据读取、导入、合并和拆分
- [Stata 新命令 readWind：快速读入并转换万德数据](http://www.jianshu.com/p/bc0582901b3a)
- [Stata: 快速读取万德 Wind 数据 (readWind2.0)](http://www.jianshu.com/p/73c5cc429ae9)
- [Stata：关于读入超大 txt 文件的讨论](http://www.jianshu.com/p/f23557e0cb1d)
- [Stata小白系列之一：调入数据](https://www.jianshu.com/p/273ab0a9f3bb)
- [Stata小白系列之二：数据拆分与合并](http://www.jianshu.com/p/8d193ffdde05)
- [使用 -import fred 命令导入联邦储备经济数据库 (FRED)](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/87e4dae27864)
- [multimport: 一次性导入及纵向合并多个文件](http://www.jianshu.com/p/4adfb9d13159)
- [Stata: 如何快速合并 3500 个无规则命名的数据文件？](https://www.jianshu.com/p/d8cc872c96f4)


#### 数据转换
- [[转]Unicode 和 UTF-8 有何区别？](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/ab72465608d8)
- [Stata15 Unicode：一次性转码，解决中文乱码问题](https://www.jianshu.com/p/4347685cc11e)
- [Stata: 字符型日期变量的转换](http://www.jianshu.com/p/88d330c5199f)
- [Stata: 物价指数 (CPI) 的导入和转换](http://www.jianshu.com/p/335dd15d4360)
- [Stata: 日收益转周\月\季\年度数据](http://www.jianshu.com/p/632a0c6a85db)
- [Stata：文字型日期格式的转换](http://www.jianshu.com/p/1db2500b02bb)
- [xtbalance 以后: 非平衡面板之转换](http://www.jianshu.com/p/f657f2d407d7)
- [Stata: 你还在用reshape转换长宽数据吗？那你就OUT了！](https://www.jianshu.com/p/a5607b37fb78)

#### 缺失值 \ 离群值 \ 重复值
- [离群值！离群值？离群值！](https://www.jianshu.com/p/0c967a1526ef)
- [Stata: 缺失值填充之 carryforward 命令](http://www.jianshu.com/p/c930b5616252)
- [Stata数据处理: 面板数据填充和补漏](http://www.jianshu.com/p/11dc82ebe94d)
- [如何处理时间序列中的日期间隔 (with gaps) 问题？](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/1c8423ee6c13)

#### 数据提取和变量生成
- [Stata：用 mapch 命令绘制“事件链”](http://www.jianshu.com/p/b90ab92db329)
- [Stata: 如何提取某个变量有记录的第一年的年份?](http://www.jianshu.com/p/aa7057eeb664)
- [Stata: 分年度-行业计算销售额前四名的行业占比](http://www.jianshu.com/p/afb4b2d75dd1)
- [Stata: gen 命令中的 group() 函数的潜在风险](http://www.jianshu.com/p/bc36ae31d847)
- [Stata: 超级强大的 gtools 命令组](http://www.jianshu.com/p/625f47cabd4b)
- [Stata: 用 astile 快速创建分组](http://www.jianshu.com/p/3b445a51f2d2)
- [Stata: 虚拟变量交乘项生成和检验的简便方法](http://www.jianshu.com/p/a0b79c552bf4)
- [Stata：runby - 一切皆可分组计算！](http://www.jianshu.com/p/68af54db9ecb)
- [Stata连享会新命令快讯：ereplace, egenmore, egenmisc](http://www.jianshu.com/p/3371e80135a4)"
- [gen 和 egen 中的 sum() 函数](https://www.jianshu.com/p/07e47dac8bd2)


#### 文件和文件夹
- [mvfiles：一次性提取多个文件夹中的文件](http://www.jianshu.com/p/6999a5e33295)
- [Stata: ftree 命令 - 用txt文档记录文件夹结构](http://www.jianshu.com/p/d8c4e2b5cae7)
- [Stata: 文本文件相关命令——合并、拆分等](http://www.jianshu.com/p/20926afe758d)
- [Stata程序: 切割文件路径和文件名](http://www.jianshu.com/p/4dc4745cf321)
- [Stata: 用 efolder 快速生成文件夹和子文件夹](http://www.jianshu.com/p/89311bf8f15e)

#### 文字变量和文本分析
- [Stata: 文本文件相关命令——合并、拆分等](http://www.jianshu.com/p/20926afe758d)
- [Stata: 正则表达式和文本分析](http://www.jianshu.com/p/033292439291)
- [Stata连享会-爬虫系列之：爬取必胜客](http://www.jianshu.com/p/3e18c87f1c97)
- [Stata: 文本分析之 tex2col 命令-文字变表格](http://www.jianshu.com/p/8df0e61f872b)

----
### 绘图

- [普林斯顿Stata教程 - Stata做图](http://www.jianshu.com/p/069bb26ad842)
- [Stata数据可视化: 十幅精美图形的绘制](http://www.jianshu.com/p/d327e3a3d3ac)
- [数据可视化：带孩子们边玩边学吧](http://www.jianshu.com/p/084c3ca3b651)
- [Stata绘图：多维柱状图绘制](https://www.jianshu.com/p/7e8517533c9c)
- [Stata绘图：bgshade - 在图形中加入经济周期阴影](http://www.jianshu.com/p/4a15727bb3af)
- [Stata绘图: 添加虚线网格线](http://www.jianshu.com/p/8a7b8e97d61b)
- [Stata: 一个干净整洁的 Stata 图形模板qlean](http://www.jianshu.com/p/3e6d4f5c0f28)
- [Stata: 用暂元统一改变图形中的字号](http://www.jianshu.com/p/fdbc6b3074f6)
- [Stata-IE-Visual-Library 可视化](http://www.jianshu.com/p/9ec8c099dbd3)
- [Stata：中文期刊风格的纯黑白图形](http://www.jianshu.com/p/425ab13feb8e)
- [Stata绘图：重新定义坐标轴刻度标签](https://www.jianshu.com/p/7b4ed2a6fd60)
- [Stata可视化：让他看懂我的结果！](https://www.jianshu.com/p/43fe2339c90c)
- [Frisch-Waugh定理与部分回归图：图示多元线性回归的系数](http://www.jianshu.com/p/54b26cfba8ae)
- [Stata连享会：动画 GIF 演示 OLS 的性质](http://www.jianshu.com/p/fe38dda826ea)
- [Stata: 姑娘的生日礼物](http://www.jianshu.com/p/55baf7d03dd3)
- [Stata: Graphing Distributions](http://www.jianshu.com/p/63b5c036f954)
- [Stata：让图片透明——你不要掩盖我的光芒](http://www.jianshu.com/p/e061f73c2a4f)
- [Stata：用 bytwoway 实现快速分组绘图](https://www.jianshu.com/p/1471cf87f25f)
- [怎么在Stata图形中附加水平线或竖直线？](https://www.jianshu.com/p/0f962953593a)
- [用 Stata 制作教学演示动态图 GIF](https://www.jianshu.com/p/d9f146c690f7)

----
### 程序\编程
- [普林斯顿Stata教程 - Stata编程](http://www.jianshu.com/p/916ddc3948d6)
- [Stata: Monte Carlo 模拟之产生符合特定分布的随机数](http://www.jianshu.com/p/42d5ba1bb245)
- [Stata: 在我的程序中接纳另一个程序的所有选项](http://www.jianshu.com/p/838f028cd95d)
- [Stata程序: 是否有类似 Python 中的 zip() 函数?](http://www.jianshu.com/p/059a1b87705d)
- [Stata程序: 切割文件路径和文件名](http://www.jianshu.com/p/4dc4745cf321)
- [Stata: 蒙特卡洛模拟(Monte Carlo Simulation)没那么神秘](http://www.jianshu.com/p/11458d032b6f)
- [极大似然估计 (MLE) 及 Stata 实现](http://www.jianshu.com/p/9fbf0589ee32)
- [Stata：GMM 简介及实现范例](http://www.jianshu.com/p/9da6fcfdfee1)
- [Stata：编写ado文件自动化执行常见任务](http://www.jianshu.com/p/3d7eb86c5a92)
- [Stata: 我的程序多久能跑完？](http://www.jianshu.com/p/6cf8675bce33)
- [如何通过 GitHub 发布自己的 Stata 命令？](http://www.jianshu.com/p/4b0b9fd44f05)
- [Stata：Mata 笔记](https://www.jianshu.com/p/03d138ff81da)
- [Stata小程序: 提取简书文章列表](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/76f18c9b96ad)
- [smclpres: 交互式展示和讲解 Stata 工具](https://www.jianshu.com/p/3c8935c7d5b6)


----

### 结果输出和呈现

- [asdoc：Stata 结果输出又一利器！](http://www.jianshu.com/p/e4ddcd06f8ae)
- [tabout: 用 Stata 输出高品质表格](http://www.jianshu.com/p/062c651d971b)
- [Stata新命令：Export tabulation results to Excel](http://www.jianshu.com/p/0e2e0d83e490)
- [Stata：今天你 “table” 了吗？](http://www.jianshu.com/p/b0537619e19c)
- [Stata：回归结果中不报告行业虚拟变量的系数](https://www.jianshu.com/p/85f09d645862)
- [Stata: 用esttab生成带组别名称的 LaTeX 回归表格](https://www.jianshu.com/p/0d327ec1f204)
- [输出相关系数矩阵至 Word / Excel 文档中：pwcorr_a 命令简介](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/5fdce59244dd)
- [君生我未生！Stata - 论文四表一键出](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/97c4f291ee1e)



### 回归分析

#### OLS\因子变量\残差\事件研究法

- [Stata连享会：动画 GIF 演示 OLS 的性质](http://www.jianshu.com/p/fe38dda826ea)
- [Stata: 实时估计个股贝塔(beta)系数](http://www.jianshu.com/p/60a0241e79e1)
- [Stata：因子变量的全攻略](http://www.jianshu.com/p/16b08797e591)
- [Stata新命令 prodest：不再畏惧生产函数！](http://www.jianshu.com/p/e3482bd7ea98)
- [获取分组回归的残差：盈余管理、过度投资的计算](http://www.jianshu.com/p/6ea640fe68f1)
- [事件研究法笔记 - Stata连享会](https://www.jianshu.com/p/46c161a44bcc)

#### 高维数据\Lasso
- [Stata: 拉索回归和岭回归 (Ridge, Lasso) 简介](http://www.jianshu.com/p/ad74fdc15499)
- [Stata新命令 pdslasso: 众多控制变量和工具变量如何挑选？](http://www.jianshu.com/p/5357a5d2220b)

#### 假设检验\标准误
- [Frisch-Waugh定理与部分回归图：图示多元线性回归的系数](http://www.jianshu.com/p/54b26cfba8ae)
- [Stata：多个变量组间均值\中位数差异检验](http://www.jianshu.com/p/3559a95dacfb)
- [Stata: 如何检验分组回归后的组间系数差异？](https://www.jianshu.com/p/38315707ef6c)
- [[旧版]Stata: 如何检验分组回归后的组间系数差异？](http://www.jianshu.com/p/f27f8716dd2d)
- [Stata 连享会: 聚类调整标准误笔记](https://www.jianshu.com/p/a096f3e3e821)
- [Stata: AIC / BIC / MSE / MAE 等信息准则的计算](http://www.jianshu.com/p/0afa0174fc6b)
- [R2 分解到每个变量上：相对重要性分析 (Dominance Analysis)](http://www.jianshu.com/p/9312835d08b4)
- [性别收入差距=歧视？Oaxaca-Blinder分解方法](http://www.jianshu.com/p/a65c1239212c)


#### 交乘项 (调节效应\中介效应)
- [如何用 Stata 做调节中介效应检验？](http://www.jianshu.com/p/9f29f5f5380f)
- [Stata: 交叉项\交乘项该这么分析！](http://www.jianshu.com/p/b5ea12da7f36)
- [Stata：交乘项该如何使用？](http://www.jianshu.com/p/f7222672fe89)
- [Stata：边际效应分析](http://www.jianshu.com/p/012d8a6159cf)
- [Stata：图示连续变量的边际效应(交乘项)](http://www.jianshu.com/p/7af58033dc24)
- [Stata：图示交互效应\调节效应](http://www.jianshu.com/p/fa6778828354)
- [加入交乘项后符号变了!?](http://www.jianshu.com/p/953f30f39195)
- [Stata: 手动计算和图示边际效应](http://www.jianshu.com/p/c6137a56b68a)
- [[转载]Stata: 使用 SEM 估计调节-中介效应](http://www.jianshu.com/p/ca914fb1f296)

#### 时间序列分析

- [Stata: VAR (向量自回归) 模型简介](http://www.jianshu.com/p/4a72c8f594f6)
- [Stata: VAR - 模拟、估计和推断](http://www.jianshu.com/p/d9ac957af894)
- [Stata：VAR 中的脉冲响应分析 (IRF)](http://www.jianshu.com/p/d55b500611a0)
- [Stata: 协整还是伪回归?](http://www.jianshu.com/p/edde2a4ca653)
- [Stata: 单位根检验就这么轻松](http://www.jianshu.com/p/ac9ffc716ff9)
- [Stata新命令dynamac：Dynamic simulation and testing of single-equation ARDL models](http://www.jianshu.com/p/2a2941630741)
- [协整：醉汉牵着一条狗](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/08e194e14b7a)

#### Logit \ Probit
- [二元选择模型：Probit 还是 Logit ？](https://www.jianshu.com/p/5111c34526b9)
- [Stata: Logit 模型简介](https://www.jianshu.com/p/251e46e40772)
- [Stata: 何时使用线性概率模型而非Logit？](http://www.jianshu.com/p/3fa3a4a7f02c)
- [Stata: 为何使用 Logit 模型？Logit 模型评介](http://www.jianshu.com/p/1c95e1f12449)
- [Stata: 用负二项分布预测蚊子存活率](http://www.jianshu.com/p/3d39d78f5cf2)
- [连享会：动态 Probit 模型及 Stata 实现](http://www.jianshu.com/p/972376c9cd21)

#### GMM \ MLE
- [GMM 简介与 Stata 实现](https://www.jianshu.com/p/f981fed94eaa)
- [极大似然估计 (MLE) 及 Stata 实现](http://www.jianshu.com/p/9fbf0589ee32)
- [Stata：GMM 简介及实现范例](http://www.jianshu.com/p/9da6fcfdfee1)

#### 面板数据模型\固定效应

- [Stata: 面板数据模型-一文读懂](http://www.jianshu.com/p/e103270ce674)
- [reghdfe: 多维固定效应估计 (尚未完成)](http://www.jianshu.com/p/4304bca04240)
- [Stata：面板中如何合理控制不可观测的异质性特征](http://www.jianshu.com/p/901494d665af)
- [Stata： 双重差分的固定效应模型 (DID)](https://www.jianshu.com/p/e97c1dc05c2c)
- [suest - 支持面板数据的似无相关检验](http://www.jianshu.com/p/0263cf0bef08)


#### 空间计量
- [Stata: 空间面板数据模型及Stata实现](http://www.jianshu.com/p/7583cfc6e29d)
- [一文遍览 Stata 官方空间计量命令: sp 系列命令](http://www.jianshu.com/p/47147a2a63b0)
- [[转载] 刘迪(空间计量): Stata空间溢出效应的动态图形 (GIF)](http://www.jianshu.com/p/7cc5899b85ca)
- [Stata: 空间计量溢出效应的动态GIF演示](http://www.jianshu.com/p/36a331cbbd32)

#### 其它

- [Stata：投入-产出表分析利器 icio](http://www.jianshu.com/p/357ec772ef92)
- [DSGE 模型的 Stata 实现](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/4579812bd0f7)
- [数据可视化：带孩子们边玩边学吧](http://www.jianshu.com/p/084c3ca3b651)
- [smclpres: 交互式展示和讲解 Stata 工具](https://www.jianshu.com/p/3c8935c7d5b6)

----
### 内生性专题 (DID, PSM, RDD 等)

#### 教材/讲义/综述
- [经典文献回顾：政策评价\因果推断的计量方法](http://www.jianshu.com/p/92ec3358c7f2)
- [一本好书：Causal Inference-Measuring the Effect of X on y](http://www.jianshu.com/p/009df5fb0540)
- [新书: The SAGE Handbook of Regression Analysis and Causal Inference](http://www.jianshu.com/p/aeef46b52e92)
- [Stata: 断点回归分析 (RDD) 文献和命令](http://www.jianshu.com/p/405eb1dff559)
- [Stata: 两本断点回归分析 (RDD) 易懂教程](http://www.jianshu.com/p/538ed1805004)
- [Abadie 新作: 简明 IV, DID, RDD 教程和综述](http://www.jianshu.com/p/2849f03a3843)"

#### 匹配方法/倾向得分匹配分析 (PSM)
- [Stata：广义精确匹配-Coarsened Exact Matching (CEM)](http://www.jianshu.com/p/f3cfc6cb0e55)
- [Stata新命令：psestimate - 倾向得分匹配中协变量的筛选](http://www.jianshu.com/p/cdf1ef2c664e)
- [Stata：配对对象的均值 (俄罗斯方块移动)](http://www.jianshu.com/p/bb4bc07cea86)
- [伍德里奇先生的问题：PSM 分析中的配对——小蝌蚪找妈妈](https://www.jianshu.com/p/6c65bb4a8aff)

#### 倍分法 (DID)
- [Stata: 倍分法(DID)板书](http://www.jianshu.com/p/c3f6cde92d5e)
- [Stata：多期倍分法 (DID) 图示](http://www.jianshu.com/p/a45879ea884c)
- [DID 边际分析：让政策评价结果更加丰满](http://www.jianshu.com/p/4ff6124df225)
- [多期DID：平行趋势检验图示](http://www.jianshu.com/p/384776721340)
- [Stata： 双重差分的固定效应模型 (DID)](https://www.jianshu.com/p/e97c1dc05c2c)
- [Fuzzy Differences-in-Differences （模糊倍分法）](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/8918037d76a1)
- [Stata新命令快讯: 有向无环图、模糊倍分法等](http://www.jianshu.com/p/3b0f925a940d)

#### 断点回归 (RDD)
- [RDD: 断点回归的非参数估计及Stata实现](http://www.jianshu.com/p/2aa62edc148b)

#### 合成控制法 (SCM)
- [合成控制法 (Synthetic Control Method) 及 Stata实现](http://www.jianshu.com/p/e4ab893b0245)
- [Stata: 合成控制法程序](http://www.jianshu.com/p/1bba82b65a78)
- [合成控制法：一组文献](http://www.jianshu.com/p/fbdebf3283d0)
- [Stata: 合成控制法 synth 命令无法加载 plugin 的解决办法](http://www.jianshu.com/p/1adc83870bbe)


### 专题教程
- [普林斯顿Stata教程 - Stata做图](http://www.jianshu.com/p/069bb26ad842)
- [普林斯顿Stata教程 - Stata数据管理](http://www.jianshu.com/p/f0aa1d0bcf79)
- [普林斯顿Stata教程 - Stata编程](http://www.jianshu.com/p/916ddc3948d6)
- [Stata小白系列之一：调入数据](https://www.jianshu.com/p/273ab0a9f3bb)
- [Stata小白系列之二：数据拆分与合并](http://www.jianshu.com/p/8d193ffdde05)

### Python \ R
- [Python-拆分文件让百万级数据运行速度提高135倍](http://www.jianshu.com/p/d5905ed7b481)

 
### Markdown 
- [连玉君 Markdown 笔记](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/db1d26af109d)
- [五分钟Markdown教程-Stata连享会](http://www.jianshu.com/p/b23deec6ff54)
- [一键将 Word 转换为 Markdown](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/df6a136d06d8)
- [Stata小程序: 提取简书文章列表](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/76f18c9b96ad)
- [在 Markdown 中使用 HTML 特殊符号](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/e65e1e36056b)
- [在 Markdown 中快速插入文字连接](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/ff3b1fa07a97)
- [神器-数学公式识别工具 mathpix](http://www.jianshu.com/p/1f0506163694)
- [Markdown 中插入图片-图床](http://www.jianshu.com/p/b90c690636fa)




### 论文写作(数学公式\LaTeX\小软件\电子板书)

- [彼此不再煎熬-如何做好毕业答辩陈述？](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/086f9cf2cc30)
- [会计和金融期刊分类](http://www.jianshu.com/p/9d6e51077eea)
- [让你的研究为人所知：10个增加学术研究影响力的方法](http://www.jianshu.com/p/75d9c7735993)
- [[转]小兵研究：中珠医疗股东发行可交换债专题研究](http://www.jianshu.com/p/e20a90f7881e)
- [实证分析者：合理规划和管理你的文档](http://www.jianshu.com/p/93ed1c584e76)
- [Finance: Top 50 Cited Articles of All Time](http://www.jianshu.com/p/8c4724ed6bc1)
- [Corporate Finance (4th): Chapter 11 Examples](https://www.jianshu.com/p/3cfd0a4b0520)
- [Corporate Finance (4th): Example 10.1](https://www.jianshu.com/p/1be821b05a09)
- [毕业论文格式和修改建议](https://www.jianshu.com/p/20654edf7228)
- [简书支持 LaTeX 数学公式了！](http://www.jianshu.com/p/d08cb0472af1)
- [EndNote X7，X8 使用说明](https://www.jianshu.com/p/af1c15c079a3)
- [Mathtype与LaTeX公式](https://www.jianshu.com/p/51dd3fff53b2)
- [最近看到的几个神器小软件](https://www.jianshu.com/p/6be12a7e4d35)
- [【乐云+百度网盘】收作业模式介绍](http://www.jianshu.com/p/2daa32e65de3)
- [屏幕涂鸦和缩放：ZoomIt (教师利器)](http://www.jianshu.com/p/76b78af068f5)
- [[转载] 手机讯飞麦克风 + 电脑讯飞输入法 = 说出你的文章](https://www.jianshu.com/p/52aa796b0c6b)
- [毕业论文格式和修改建议](https://www.jianshu.com/p/20654edf7228)
- [可汗学院风格电子板书攻略: Wacom+ArtRage](https://www.jianshu.com/p/717b2e689d96)
- [最近看到的几个神器小软件](https://www.jianshu.com/p/6be12a7e4d35)
- [码云：我把常用小软件都放这儿了](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/fbb6bdeb9df5)
- [一个博士生该掌握哪些基本工具(武器)？](https://link.zhihu.com/?target=http%3A//www.jianshu.com/p/90d6a54e35a5)
- [连玉君的链接](http://www.jianshu.com/p/bdc6ab7747f8)


### 过往课程
- [助教名单公布: 2019 连享会计量方法与经典论文专题班](http://www.jianshu.com/p/5e837038af77)
- [诚邀助教-2019暑期Stata现场班](http://www.jianshu.com/p/ac22dfdd1e03)
- [2019 Stata寒假班：做助教，免费听课](http://www.jianshu.com/p/0ba6b0501737)
- [Stata连享会2018年11月现场培训-内生性专题](http://www.jianshu.com/p/db38baff91be)
- [Stata连享会2018.11现场班-助教名单公布](http://www.jianshu.com/p/2217924d5126)
- [2018暑期 - Stata现场班（北京、山西、广东）](http://www.jianshu.com/p/af6fb0448297)
- [连玉君主讲-2018暑期Stata研讨班(7.23-8.2)](http://www.jianshu.com/p/c195889e3bc0)
- [2018暑期Stata现场培训-山西财大专场(2018.8.3-8)](https://www.jianshu.com/p/b871a928709b)


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![](https://images.gitee.com/uploads/images/2019/0805/101729_fb6eb53e_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)


---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/1213/080925_6316cb36_1522177.jpeg "扫码关注 Stata 连享会")
